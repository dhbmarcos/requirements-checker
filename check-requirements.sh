#!/bin/bash

readonly EXIT_SUCCESS=0
readonly EXIT_FAILURE=1

function print_red
{
    local readonly text="$1"

    if [ -t 1 ]
    then
        echo -ne "\e[31m${text}\e[0m";
    else
        echo -ne "${text}";
    fi;
}

function print_green
{
    local readonly text="$1"

    if [ -t 1 ]
    then
        echo -ne "\e[32m${text}\e[0m";
    else
        echo -ne "${text}";
    fi;
}

function print_bold
{
    local readonly text="$1"

    if [ -t 1 ]
    then
        echo -ne "\e[1m${text}\e[0m";
    else
        echo -ne "${text}";
    fi;
}

if [ "$1" == "--help" ] || [ "$1" == "-h" ]
then
    echo "Check requirements from files in current directory.";
    echo;
    echo "To check a requirement, create a file according format below and run '$0'.";
    echo "Pay attention to add ASCII Line Feed character (\n) at end of file.";
    echo;
    print_bold "File Exist:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.exist'.";
    echo "  The file content, in first line, is the path of file.";
    echo;
    print_bold "Regular File:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.regular'.";
    echo "  The file content, in first line, is the path of file.";
    echo;
    print_bold "Executable File:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.executable'.";
    echo "  The file content, in first line, is the path of executable file.";
    echo;
    print_bold "Symbolic Link:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.link'.";
    echo "  The file content, in first line, is the path of symbolic link.";
    echo;
    print_bold "Directory:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.directory'.";
    echo "  The file content, in first line, is the path of directory.";
    echo;
    print_bold "Named Pipe:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.pipe'.";
    echo "  The file content, in first line, is the path of named pipe.";
    echo;
    print_bold "Flow Character Device Port:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.character'.";
    echo "  The file content, in first line, is the path of flow character device port.";
    echo;
    print_bold "Block Device Port:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.block'.";
    echo "  The file content, in first line, is the path of block device port.";
    echo;
    print_bold "SystemD Unit:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.unit'.";
    echo "  The file content is:";
    echo "    in first line, is the name of unit including type o unit suffix.";
    echo "    in second line, is the expected active state of unit.";
    echo "    in third line, is the expected load state of unit.";
    echo;
    print_bold "Debian Package:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.debian'.";
    echo "  The file content, in first line, is the name of Debian package.";
    echo;
    print_bold "Python Package:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.python'.";
    echo "  The file content, in first line, is the name of Pyhton package.";
    echo;
    print_bold "Opened Port:";
    echo;
    echo "  Create a file in current directory with suffix (extension) '.port'.";
    echo "  The file content is:";
    echo "    in first line, is the IP address.";
    echo "    in second line, is the port.";
    echo;

    exit $EXIT_SUCCESS;
fi;

requirements_compliant=true

function print_color_result
{
    local readonly result="$1"

    if [ "${result}" == "true" ]
    then
        if [ -t 1 ]
        then
            echo -n " [  ";
            print_green "OK";
            echo -n "  ] ";
        else
            echo " [  OK  ] ";
        fi;
    else
        if [ -t 1 ]
        then
            echo -n " [ ";
            print_red "FAIL";
            echo -n " ] ";
        else
            echo " [ FAIL ] ";
        fi;
    
        requirements_compliant=false
    fi;
}

function check_exist_files
{
    for file_to_check in ${exist_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -f "${file_path}" ]
        then
            print_color_result true;
            echo "File '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' not found. Check file '${file_to_check}'."
        fi;
    done
}

function check_block_files
{
    for file_to_check in ${block_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -b "${file_path}" ]
        then
            print_color_result true;
            echo "Block '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a block. Check file '${file_to_check}'."
        fi;
    done
}

function check_character_files
{
    for file_to_check in ${character_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -c "${file_path}" ]
        then
            print_color_result true;
            echo "Character flow '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a character flow. Check file '${file_to_check}'."
        fi;
    done
}

function check_directory_files
{
    for file_to_check in ${directory_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -d "${file_path}" ]
        then
            print_color_result true;
            echo "Directory '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a directory. Check file '${file_to_check}'."
        fi;
    done
}

function check_regular_files
{
    for file_to_check in ${regular_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -f "${file_path}" ]
        then
            print_color_result true;
            echo "Regular file '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a regular file. Check file '${file_to_check}'."
        fi;
    done
}

function check_link_files
{
    for file_to_check in ${link_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -h "${file_path}" ]
        then
            print_color_result true;
            echo "Symbolic link '${file_path}' to '$(readlink "${file_path}")' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a symbolic link. Check file '${file_to_check}'."
        fi;
    done
}

function check_pipe_files
{
    for file_to_check in ${pipe_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -p "${file_path}" ]
        then
            print_color_result true;
            echo "Named pipe '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a named pipe. Check file '${file_to_check}'."
        fi;
    done
}

function check_executable_files
{
    for file_to_check in ${executable_files[@]}
    do
        file_path="$(cat ${file_to_check} | head -1)"
        if ((${#file_path} == 0))
        then
            continue;
        fi;

        if [ -x "${file_path}" ]
        then
            print_color_result true;
            echo "Excutable '${file_path}' found.";
        else
            print_color_result false;
            echo "'${file_path}' is not a executable. Check file '${file_to_check}'."
        fi;
    done
}

function check_debian_packages
{
    for package in ${packages[@]}
    do
        name="$(cat ${package} | head -1)"
        if ((${#name} == 0))
        then
            continue;
        fi;

        data="$(dpkg -s ${name} 2>/dev/null)"
        description="$(echo -e "${data}" | grep 'Description:' | cut -d: -f2 | xargs)"
        status="$(echo -e "${data}" | grep 'Status:' | cut -d: -f2 | xargs)"

        if [ "${status}" == "install ok installed" ]
        then
            print_color_result true;
            echo "Debian package '${name}' (${description}) is installed.";
        else
            if ((${#status} == 0))
            then
                status="not installed"
            fi;

            print_color_result false;
            echo "Debian package '${name}' is non-compliance installed (${status}). Check file '${package}'."
        fi;
    done
}

function check_python_packages
{
    for package in ${python_package[@]}
    do
        name="$(cat ${package} | head -1)"
        if ((${#name} == 0))
        then
            continue;
        fi;
        
        data="$(pip show ${name} 2>/dev/null)"
        description="$(echo -e "${data}" | grep 'Summary: ' | cut -d: -f2 | xargs)"
        version="$(echo -e "${data}" | grep 'Version: ' | cut -d: -f2 | xargs)"

        if ((${#version} > 0))
        then
            print_color_result true;
            echo "Python package '${name}' (${description}) is installed.";
        else
            print_color_result false;
            echo "Python package '${name}' is non-compliance installed (not installed). Check file '${package}'."
        fi;
    done
}

function check_units
{
    for unit in ${units[@]}
    do
        name="$(cat ${unit} | head -1)"

        if (( $(wc -l ${unit} | cut -d' ' -f1) > 1 ))
        then
            expected_active_state="$(cat ${unit} | head -2 | tail -1)"
        fi;

        if (( $(wc -l ${unit} | cut -d' ' -f1) > 2 ))
        then
            expected_load_state="$(cat ${unit} | head -3 | tail -1)"
        fi;

        if ((${#name} == 0))
        then
            continue;
        fi;

        if ((${#expected_active_state} == 0))
        then
            expected_active_state='active'
        fi;

        if ((${#expected_load_state} == 0))
        then
            expected_load_state='loaded'
        fi;

        data="$(systemctl show ${name} 2>/dev/null)"
        description="$(echo -e "${data}" | grep 'Description=' | cut -d= -f2 | xargs)"
        active_state="$(echo -e "${data}" | grep 'ActiveState=' | cut -d= -f2 | xargs)"
        load_state="$(echo -e "${data}" | grep 'LoadState=' | cut -d= -f2 | xargs)"

        if [ "${active_state}" == "${expected_active_state}" ] && [ "${load_state}" == "${expected_load_state}" ]
        then
            print_color_result true;
            echo "Unit '${name}' (${description}) is ${expected_active_state} and ${expected_load_state}.";
        else
            print_color_result false;
            echo "Unit '${name}' is non-compliance (${active_state}, ${load_state}). Check file '${unit}'."
        fi;
    done
}

function check_ports
{
    for port_file in ${ports[@]}
    do
        port="$(cat ${port_file} | head -1)"
        if ((${#port} == 0))
        then
            continue;
        fi;

        if (( $(wc -l ${port_file} | cut -d' ' -f1) > 1 ))
        then
            ip="$(cat ${port_file} | head -2 | tail -1)"
        else
            ip=127.0.0.1
        fi;        
        
        nc -zvw10 $ip $port &> /dev/null;        
        result=$?

        if (($result == 0))
        then
            print_color_result true;
            echo "Service '${ip}:${port}' is accessible from this host.";
        else
            print_color_result false;
            echo "Service '${ip}:${port}' is inaccessible from this host. Check file '${port_file}'.";
        fi;
    done
}

exist_files=($(ls *.exist 2> /dev/null))
regular_files=($(ls *.regular 2> /dev/null))
executable_files=($(ls *.executable 2> /dev/null))
link_files=($(ls *.link 2> /dev/null))
directory_files=($(ls *.directory 2> /dev/null))
pipe_files=($(ls *.pipe 2> /dev/null))
character_files=($(ls *.character 2> /dev/null))
block_files=($(ls *.block 2> /dev/null))
units=($(ls *.unit 2> /dev/null))
packages=($(ls *.debian 2> /dev/null))
python_package=($(ls *.python 2> /dev/null))
ports=($(ls *.port 2> /dev/null))

check_exist_files;
check_regular_files;
check_executable_files;
check_link_files;
check_directory_files;
check_pipe_files;
check_character_files;
check_block_files;
check_units;
check_debian_packages;
check_python_packages;
check_ports;

if $requirements_compliant
then
    print_green "All requirements are compliant.";
    echo;
    exit $EXIT_SUCCESS;
else
    print_red "There are non-compliance requirements.";
    echo;
    exit $EXIT_FAILURE;
fi;
